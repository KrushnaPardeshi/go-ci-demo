package main

import (
	"fmt"

	"github.com/kyokomi/emoji"
)

func main() {
	fmt.Println(sayHello("Ronnie"))
	fmt.Println(sayHelloFrench("Jean"))
}

func sayHello(name string) string {
	return emoji.Sprint("Hello, ", name, "! :wink:")
}

func sayHelloFrench(name string) string {
	return emoji.Sprint("Bonjour, ", name, "! :heart:")
}
